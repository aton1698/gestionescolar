import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import {HttpClientModule} from "@angular/common/http";
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// imports from the necessary services
import {AssistanceService} from "./services/assistance.service";
import {DocenteService} from "./services/docente.service";
import {StudentService} from "./services/student.service";
import {groupModelService} from "./services/group.service";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// imports for angular material
import {MatButtonModule, MatIconModule, MatTableModule, MatToolbarModule, MatCheckboxModule, MatNativeDateModule} from '@angular/material';
import {MatIcon} from "@angular/material/icon";
import {LoginComponent} from "./login/login.component";
import {FormsModule} from "@angular/forms";


@NgModule({
  declarations: [AppComponent, LoginComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatNativeDateModule, FormsModule,
  ],
  providers: [
    StatusBar,
      AssistanceService,
      DocenteService,
      StudentService,
      groupModelService,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
