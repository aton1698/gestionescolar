export class Assistance {
  constructor(
    // tslint:disable-next-line:variable-name
    public id_estudiante: number,
    // tslint:disable-next-line:variable-name
    public id_grupo: number,
    public dia: number,
    public mes: number,
    public anio: number,
    // tslint:disable-next-line:variable-name
    public ausencia_justificada: boolean,
    // tslint:disable-next-line:variable-name
    public ausencia_injustificada: boolean,
    // tslint:disable-next-line:variable-name
    public tardia_justificada: boolean,
    // tslint:disable-next-line:variable-name
    public tardia_injustificada: boolean,
    public presente: boolean,
    public genero: string,
    // tslint:disable-next-line:variable-name
    public nombre_estudiante: string
  ) { }
}
