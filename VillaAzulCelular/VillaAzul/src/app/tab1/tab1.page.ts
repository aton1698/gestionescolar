import {Component, OnInit} from '@angular/core';
import {UserService} from "../services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit{

  group: string;
  teacherName; string;
  year = new Date().getFullYear();

  ngOnInit() {
    this.group = this.userService.getUserLoggedIn().currentGroupName;
    this.teacherName = this.userService.getUserLoggedIn().username;
  }

  constructor(private userService: UserService, private router: Router) {
    if (!this.userService.getUserLoggedIn()) {
      this.router.navigateByUrl('/login');
    }
  }

  close() {
    this.userService.exit();
    this.router.navigateByUrl('/login');
  }
}
