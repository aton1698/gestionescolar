import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { groupModel} from '../model/groupModel';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})

// tslint:disable-next-line:class-name
export class groupModelService {
  // Base url
  // baseurl = 'http://localhost:3000';
  baseurl = 'http://192.168.1.212:3000';

  constructor(private http: HttpClient) { }

  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // POST
  // This function receives the information for the groupModel and creates the http post request for the API
  // TODO Figure out how does this receives the data, is it a groupModel object?
  CreategroupModel(data): Observable<groupModel> {
    console.log(this.baseurl + '/api/grupos', JSON.stringify(data));
    return this.http.post<groupModel>(this.baseurl + '/api/grupos', JSON.stringify(data), this.httpOptions)
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }

  // GET
  // Function that receives a specific ID and returns the groupModel for that ID
  GetgroupModel(id): Observable<groupModel> {
    console.log(this.baseurl + '/api/grupos' + id);
    return this.http.get<groupModel>(this.baseurl + '/api/grupos' + id)
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }

  // GET
  // Function that receives a specific ID and returns the ID info for the courses that teacher has
  GetClassesGroupModel(groupModelId): Observable<groupModel> {
    console.log(this.baseurl + '/api/grupos' + groupModelId + '/gruposgroupModel');
    return this.http.get<groupModel>(this.baseurl + '/api/grupos' + groupModelId + '/gruposgroupModel')
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }



  // GET
  // Function that returns all the groupModels in the database
  GetgroupModels(): Observable<groupModel> {
    console.log(this.baseurl + '/api/grupos');
    return this.http.get<groupModel>(this.baseurl + '/api/grupos')
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }

  // GET
  // Function that returns all the assistance that belong to a specific group
  GetgroupModelsById(groupId): Observable<groupModel> {
    console.log('/api/grupos/' + groupId + '/asistenciaGrupo');
    return this.http.get<groupModel>(this.baseurl + '/api/grupos/' + groupId + '/asistenciaGrupo')
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }




  // GET
  // Function that returns all the assistnace that belong to a specific group and filtered by a specific month
  GetgroupsByIdAndMonth(groupId , month): Observable<groupModel> {
    console.log('/api/grupos/' + groupId + '/asistenciaGrupo?filter=%7B%22where%22%3A%7B%22mes%22%3A%22' + month + '%22%7D%7D');
    return this.http.get<groupModel>(this.baseurl + '/api/grupos/' + groupId + '/asistenciaGrupo?filter=%7B%22where%22%3A%7B%22mes%22%3A%22' + month + '%22%7D%7D')
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }

  // GET
  // Function that returns all the assistnace that belong to a specific group and filtered by a specific year
  GetgroupsByIdAndYear(groupId , year): Observable<groupModel> {
    console.log('/api/grupos/' + groupId + '/asistenciaGrupo?filter=%7B%22where%22%3A%7B%22anio%22%3A%22' + year + '%22%7D%7D');
    return this.http.get<groupModel>(this.baseurl + '/api/grupos/' + groupId + '/asistenciaGrupo?filter=%7B%22where%22%3A%7B%22anio%22%3A%22' + year + '%22%7D%7D')
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }

  GetgroupsByFilter(groupId , year): Observable<groupModel> {
    console.log('/api/grupos/' + groupId + '/asistenciaGrupo?filter=%7B%22where%22%3A%7B%22anio%22%3A%22' + year + '%22%7D%7D');
    return this.http.get<groupModel>(this.baseurl + '/api/grupos/' + groupId + '/asistenciaGrupo?filter=%7B%22where%22%3A%7B%22anio%22%3A%22' + year + '%22%7D%7D')
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }

  // PUT
  UpdategroupModel(id, data): Observable<groupModel> {
    console.log(this.baseurl + '/api/grupos' + id, JSON.stringify(data));
    return this.http.put<groupModel>(this.baseurl + '/api/grupos' + id, JSON.stringify(data), this.httpOptions)
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }

  // DELETE
  // Function that receives an ID and deletes the groupModel with that ID
  DeletegroupModel(id) {
    console.log(this.baseurl + '/api/grupos' + id);
    return this.http.delete<groupModel>(this.baseurl + '/api/grupos' + id, this.httpOptions)
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }

  // Error handling
  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
