import { Injectable } from '@angular/core';
import { User } from '../user/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public isUserLoggedIn;
  public usserLogged: User;

  constructor() {
    this.isUserLoggedIn = false;
  }
  setUserLoggedIn(user: User) {
    this.isUserLoggedIn = true;
    this.usserLogged = user;
    localStorage.setItem('currentUser', JSON.stringify(user));
    console.log(JSON.stringify(user));
  }
  exit() {
    this.isUserLoggedIn = false;
    localStorage.clear();
  }

  getUserLoggedIn() {
    return JSON.parse(localStorage.getItem('currentUser'));
  }
}

