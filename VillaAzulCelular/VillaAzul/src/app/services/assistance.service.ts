import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Assistance} from '../model/assistance';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import {Student} from '../model/student';


@Injectable({
  providedIn: 'root'
})

export class AssistanceService {
  // Base url
  // baseurl = 'http://localhost:3000';
  baseurl = 'http://192.168.1.212:3000';

  constructor(private http: HttpClient) { }

  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // POST
  // This function receives the information for the Assistance and creates the http post request for the API
  // TODO Figure out how does this receives the data, is it a Assistance object?
  CreateAssistance(data): Observable<Assistance> {
    console.log(this.baseurl + '/api/asistencia', JSON.stringify(data));
    return this.http.post<Assistance>(this.baseurl + '/api/asistencia', JSON.stringify(data), this.httpOptions)
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }

  // GET
  // Function that receives a specific ID and returns the Assistance for that ID
  GetAssistance(id): Observable<Assistance> {
    console.log(this.baseurl + '/api/asistencia/' + id);
    return this.http.get<Assistance>(this.baseurl + '/api/asistencia/' + id)
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }
  // GET
  // Function that returns all the Assistances in the database
  GetAssistances(): Observable<Assistance> {
    console.log(this.baseurl + '/api/asistencia');
    return this.http.get<Assistance>(this.baseurl + '/api/asistencia')
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }


  // GET
  // Function that returns all the Assistances in a group given by a group id, and a filter (day,month,year,etc) and a target valid from that filter
  GetAssistanceFilters(groupId, filter, target): Observable<Assistance> {
    console.log(this.baseurl + '/api/asistencia?filter=%7B%22where%22%3A%20%7B%22and%22%3A%5B%7B%22id_grupo%22%3A' + groupId + '%7D%2C%7B%22' + filter + '%22%3A' + target + '%7D%5D%7D%7D');
    return this.http.get<Assistance>(this.baseurl + '/api/asistencia?filter=%7B%22where%22%3A%20%7B%22and%22%3A%5B%7B%22id_grupo%22%3A' + groupId + '%7D%2C%7B%22' + filter + '%22%3A' + target + '%7D%5D%7D%7D')
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }


  // PUT
  UpdateAssistance(id, data): Observable<Assistance> {
    console.log(this.baseurl + '/api/asistencia' + id, JSON.stringify(data));
    return this.http.put<Assistance>(this.baseurl + '/api/asistencia' + id, JSON.stringify(data), this.httpOptions)
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }

  // DELETE
  // Function that receives an ID and deletes the Assistance with that ID
  DeleteAssistance(id) {
    console.log(this.baseurl + '/api/asistencia/' + id);
    return this.http.delete<Assistance>(this.baseurl + '/api/asistencia/' + id, this.httpOptions)
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }

  // Error handling
  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
