import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Student, StudentID} from '../model/student';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})

export class StudentService {
  // Base url
  // baseurl = 'http://localhost:3000';
  baseurl = 'http://192.168.1.212:3000';
  constructor(private http: HttpClient) { }

  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // POST
  // This function receives the information for the Student and creates the http post request for the API
  // TODO Figure out how does this receives the data, is it a Student object?
  public CreateStudent(data): Observable<Student> {
    console.log(JSON.stringify(data));
    return this.http.post<Student>(this.baseurl + '/api/estudiantes', JSON.stringify(data), this.httpOptions)
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }

  // GET
  // Function that receives a specific ID and returns the Student for that ID
  GetStudent(id): Observable<Student> {
    console.log(this.baseurl + '/api/estudiantes/' + id);
    return this.http.get<Student>(this.baseurl + '/api/estudiantes/' + id)
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }

  // GET
  // Function that returns all the Students in the database
  GetStudents(): Observable<Student[]> {
    console.log(this.baseurl + '/api/estudiantes');
    return this.http.get<Student[]>(this.baseurl + '/api/estudiantes')
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }

  // GET
  // Function that returns all the Students in the database based on filter and target
  GetStudentsFilter(filter, target): Observable<Student> {
    console.log(this.baseurl + '/api/estudiantes?filter=%7B%22where%22%3A%7B%22' + filter + '%22%3A%22' + target + '%22%7D%7D');
    return this.http.get<Student>(this.baseurl + '/api/estudiantes?filter=%7B%22where%22%3A%7B%22' + filter + '%22%3A%22' + target + '%22%7D%7D')
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }

  // GET
  // Function that returns all the grades from a student from the database
  GetGradesStudent(id): Observable<Student> {
    console.log('/api/estudiantes/' + id + '/materias');
    return this.http.get<Student>(this.baseurl + '/api/estudiantes/' + id + '/materias')
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }

  // GET
  // Function that returns all the students that are in a specific group
  GetStudentsFromGroup(id): Observable<Student> {
    console.log(this.baseurl + '/api/grupos/' + id + '/estudiantesGrupo');
    return this.http.get<Student>(this.baseurl + '/api/grupos/' + id + '/estudiantesGrupo')
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }

  // PUT
  // Function that updates the info of a specific student using his ID and receiving the student data
  UpdateStudent(id, data): Observable<StudentID> {
    console.log(this.baseurl + '/api/estudiantes/' + id + '/replace', JSON.stringify(data));
    return this.http.post<StudentID>(this.baseurl + '/api/estudiantes/' + id + '/replace', JSON.stringify(data), this.httpOptions)
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }

  // DELETE
  // Function that receives an ID and deletes the Student with that ID
  DeleteStudent(id) {
    console.log(this.baseurl + '/api/estudiantes' + id);
    return this.http.delete<Student>(this.baseurl + '/api/estudiantes' + id, this.httpOptions)
        .pipe(
            retry(1),
            catchError(this.errorHandl)
        );
  }

  // Error handling
  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
