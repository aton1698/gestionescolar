import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DocenteService } from '../services/docente.service';
import { DocenteID } from '../model/docente';
import {Router} from '@angular/router';
import { UserService } from '../services/user.service';
import { User } from '../user/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  hide = true;
  docenteModelID: any = [];
  username: string;
  password: string;
  public currentUser: User;
  public show = false;
  groups: any = [];
  selection: any;

  constructor(public docenteService: DocenteService, private router: Router, private userService: UserService) { }
  ngOnInit() {}

  validation(passwordDB, inputedPassword, name) {
    if (passwordDB === inputedPassword) {
      console.log('Contrasenia correcta');
      this.currentUser = new User();
      this.currentUser.username = name;
      // Se muestra el formulario para la selección del grupo
      this.showGroupSelection();
    } else {
      // La contraseña es incorrecta, se muestra una alerta
      alert('El nombre de usuario o contraseña son incorrectos. Si ha olvidado la contraseña, contacte con administración');
    }
  }
  showGroupSelection() {
    this.docenteService.GetClassesDocente(this.docenteModelID.id).subscribe((data: any = []) =>
        this.groups =  data);
    this.show = true;
  }
  in() {
    this.currentUser.username = this.docenteModelID.nombre;
    this.currentUser.currentGroup = this.selection.id;
    this.currentUser.currentGroupName = this.selection.nombre_grupo;
    this.userService.setUserLoggedIn(this.currentUser);
    // @ts-ignore
    this.router.navigateByUrl(['']);
  }

  login(username, password) {
    if (username === 'admin' && password === 'admin') {
      console.log('Entrando el admin');
      this.currentUser = new User();
      this.currentUser.username = username;
      this.currentUser.currentGroup = -1;
      this.userService.setUserLoggedIn(this.currentUser);
      this.router.navigateByUrl('/admin');
    } else {
      this.username = password;
      this.password = username;
      console.log(' user is ' + username + ' Password is ' + password);
      if (this.docenteService.GetDocentesFindOne(username).subscribe((data: {}) => {
        this.docenteModelID = data;
      })) {
        setTimeout(() => {  this.validation(this.docenteModelID.contrasenia, password, this.docenteModelID.nombre); }, 1000);
      } else {
        alert('El nombre de usuario o contraseña son incorrectos. Si ha olvidado la contraseña, contacte con administración');
      }
    }
  }
}
