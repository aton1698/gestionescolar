import { Component, OnInit } from '@angular/core';
import {UserService} from "../services/user.service";
import {Router} from "@angular/router";
import {MatTableDataSource} from "@angular/material/table";
import {StudentService} from "../services/student.service";

@Component({
  selector: 'app-delete-student',
  templateUrl: './delete-student.component.html',
  styleUrls: ['./delete-student.component.css']
})
export class DeleteStudentComponent implements OnInit {

  nombre: string;
  show = false;
  students: any = [];
  exist = true;

  displayedColumns: string[] = ['nombre', 'apellido1', 'apellido2', 'sexo', 'fecha_admision', 'accion'];

  constructor(public studentService: StudentService, private userService: UserService, private router: Router) { }
  search(nombre) {
    if ( this.studentService.GetStudentsFilter('nombre', nombre).subscribe((data: any = []) => this.students =  new MatTableDataSource(data))) {
      this.show = true;
    } else {
      this.exist = false;
    }
    //setTimeout(() => {  this.show = true; }, 3000);
  }
  delete(element) {
    this.studentService.DeleteStudent(element.id).subscribe(res => {console.log('Estudiante eliminado'); });
    alert( element.nombre + ' ha sido eliminado del sistema' );
    window.location.reload();
  }

  ngOnInit() {
  }

}
