import { Component, OnInit } from '@angular/core';
import {ofertaCursosService} from '../services/ofertaCursosService';
import {ofertaCursosID} from '../model/ofertaCursos';
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-delete-course',
  templateUrl: './delete-course.component.html',
  styleUrls: ['./delete-course.component.css']
})
export class DeleteCourseComponent implements OnInit {
  curso: any = [];
  nombre: string;
  show = false;
  exist = true;

  displayedColumns: string[] = ['nombre', 'accion'];

  // tslint:disable-next-line:no-shadowed-variable
  constructor(public ofertaCursosService: ofertaCursosService) { }


  delete(element) {
    this.ofertaCursosService.DeleteofertaCurso(element.id).subscribe(res => {console.log('Curso eliminado'); });
    alert( element.nombre + ' ha sido eliminado del sistema' );
    window.location.reload();
  }

  ngOnInit() {
    if ( this.ofertaCursosService.GetofertaCursos().subscribe((data: any = []) => this.curso = data)) {
      this.show = true;
    } else {
      this.exist = false;
    }
  }

}
