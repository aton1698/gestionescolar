import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnualAssistanceComponent } from './annual-assistance.component';

describe('AnnualAssistanceComponent', () => {
  let component: AnnualAssistanceComponent;
  let fixture: ComponentFixture<AnnualAssistanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnualAssistanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnualAssistanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
