import { Component, OnInit } from '@angular/core';
import {Student} from '../model/student';
import {UserService} from '../services/user.service';
import {AssistanceService} from '../services/assistance.service';
import {StudentService} from '../services/student.service';
import {Assistance} from '../model/assistance';
import * as jspdf from 'jspdf';

@Component({
  selector: 'app-annual-assistance',
  templateUrl: './annual-assistance.component.html',
  styleUrls: ['./annual-assistance.component.css']
})
export class AnnualAssistanceComponent implements OnInit {

  year = new Date().getFullYear();
  group;
  totalAusencias = 0;
  ausenciasJustificadasH = 0;
  ausenciasJustificadasM = 0;
  ausenciasInjustificadasH = 0;
  ausenciasInjustificadasM = 0;

  totalTardias = 0;
  tardiasJustificadasH = 0;
  tardiasJustificadasM = 0;
  tardiasInjustificadasH = 0;
  tardiasInjustificadasM = 0;
  dataSource: any = [];
  students: Student;
  loading = false;

  constructor(private userService: UserService, public assistanceService: AssistanceService, public studentService: StudentService) {
  }

  ngOnInit() {
    this.group = this.userService.getUserLoggedIn().currentGroupName;
  }

  generar() {
    const doc = new jspdf();
    // It can parse html:
    // doc.autoTable({ html: '#my-table' });
    console.log('hola');
    // Or use javascript directly:
    doc.autoTable({
      head: [[this.year + ' ' + this.group, 'Total', 'H', 'M']],
      body: [
        ['Ausencias Motivadas', this.ausenciasJustificadasM + this.ausenciasJustificadasH, this.ausenciasJustificadasH, this.ausenciasJustificadasM],
        ['Ausencias Inmotivadas', this.ausenciasInjustificadasM + this.ausenciasInjustificadasH, this.ausenciasInjustificadasH, this.ausenciasJustificadasM],
        ['Total Ausencias', this.totalAusencias, this.ausenciasJustificadasH + this.ausenciasInjustificadasH, this.ausenciasJustificadasM + this.ausenciasInjustificadasM ],
        ['Tardías Motivadas', this.tardiasJustificadasM + this.tardiasJustificadasH, this.tardiasJustificadasH, this.tardiasJustificadasM],
        ['Tardías Inmotivadas', this.tardiasInjustificadasM + this.tardiasInjustificadasH, this.tardiasInjustificadasH, this.tardiasJustificadasM],
        ['Total Tardías', this.totalTardias, this.tardiasJustificadasH + this.tardiasInjustificadasH, this.tardiasJustificadasM + this.tardiasInjustificadasM],
      ],
    });

    doc.save('Asistencia anual ' + this.group + '.pdf');
    this.loading = false;
    this.resetValues();
    alert('Reporte generado con éxito, presione aceptar para iniciar la descarga');
  }
  log() {
    this.loading = true;
    // const month = new Date().getMonth() + 1;
    this.assistanceService.GetAssistanceFilters(this.userService.getUserLoggedIn().currentGroup, 'anio', this.year).subscribe((data: any = []) =>
      this.dataSource =  data);
    setTimeout(() => {  this.completeData(); }, 3000);
    setTimeout(() => {  this.generar(); }, 9000);
  }
  completeData() {
    // tslint:disable-next-line:forin
    for (const index in this.dataSource) {
      setTimeout(() => { this.defineData(this.dataSource[index]); console.log('En for, con el estudiante: ' + this.dataSource[index].id_estudiante); }, 3000);
    }
  }
  defineData(currentAssitance: Assistance) {
    this.studentService.GetStudent(currentAssitance.id_estudiante).subscribe((data: Student) =>
      this.students =  data);
    setTimeout(() => {
      if (this.students.sexo === ' Femenino') {
        if (currentAssitance.tardia_injustificada) {
          this.tardiasInjustificadasM += 1;
          this.totalTardias += 1;
        } else if (currentAssitance.tardia_justificada) {
          this.tardiasJustificadasM += 1;
          this.totalTardias += 1;
        } else if (currentAssitance.ausencia_injustificada) {
          this.ausenciasInjustificadasM += 1;
          this.totalAusencias += 1;
        } else if (currentAssitance.ausencia_justificada) {
          this.ausenciasJustificadasM += 1;
          this.totalAusencias += 1;
        }
      } else {
        if (currentAssitance.tardia_injustificada) {
          this.tardiasInjustificadasH += 1;
          this.totalTardias += 1;
        } else if (currentAssitance.tardia_justificada) {
          this.tardiasJustificadasH += 1;
          this.totalTardias += 1;
        } else if (currentAssitance.ausencia_injustificada) {
          this.ausenciasInjustificadasH += 1;
          this.totalAusencias += 1;
        } else if (currentAssitance.ausencia_justificada) {
          this.ausenciasJustificadasH += 1;
          this.totalAusencias += 1;
        }
      } }, 100);
  }

  resetValues() {
    this.dataSource = [];
    this.totalAusencias = 0;
    this.ausenciasJustificadasH = 0;
    this.ausenciasJustificadasM = 0;
    this.ausenciasInjustificadasH = 0;
    this.ausenciasInjustificadasM = 0;

    this.totalTardias = 0;
    this.tardiasJustificadasH = 0;
    this.tardiasJustificadasM = 0;
    this.tardiasInjustificadasH = 0;
    this.tardiasInjustificadasM = 0;
  }
}
