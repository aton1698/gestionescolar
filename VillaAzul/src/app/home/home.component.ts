import { Component, OnInit } from '@angular/core';
import {UserService} from '../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  group: string;
  teacherName; string;
  year = new Date().getFullYear();

  constructor(private userService: UserService, private router: Router) {
    if (!this.userService.getUserLoggedIn() || this.userService.getUserLoggedIn().username === 'admin') {
      this.router.navigateByUrl('/login');
    }
  }
  ngOnInit() {
    this.group = this.userService.getUserLoggedIn().currentGroupName;
    this.teacherName = this.userService.getUserLoggedIn().username;
  }

}
