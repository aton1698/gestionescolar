import { Component, OnInit } from '@angular/core';

import {groupModelService} from '../services/group.service';
import {DocenteService} from "../services/docente.service";
import {groupModel} from "../model/groupModel";


@Component({
  selector: 'app-teacher-assignment',
  templateUrl: './teacher-assignment.component.html',
  styleUrls: ['./teacher-assignment.component.css']
})
export class TeacherAssignmentComponent implements OnInit {

  group: any;
  teacherID: number;
  dataGoups: any = [];
  dataTeachers: any = [];

  constructor(public groupService: groupModelService, public docenteService: DocenteService) { }

  ngOnInit() {
    this.groupService.GetgroupModels().subscribe((data: any = []) =>
      this.dataGoups =  data);
    this.docenteService.GetDocentes().subscribe((data: any = []) =>
      this.dataTeachers =  data);
  }

  updateGroup() {
    const newGroup = new groupModel(this.group.id, this.teacherID, this.group.nombre_grupo);
    this.groupService.UpdategroupModel(this.group.id, newGroup).subscribe(res => {console.log('Grupo asignado'); });
    alert( 'El grupo ha sido asignado' );
  }

}
