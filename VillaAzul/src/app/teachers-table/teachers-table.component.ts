import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {DocenteService} from '../services/docente.service';

@Component({
  selector: 'app-teachers-table',
  templateUrl: './teachers-table.component.html',
  styleUrls: ['./teachers-table.component.css']
})
export class TeachersTableComponent implements OnInit {

  displayedColumns: string[] = ['nombre', 'apellido1', 'apellido2', 'usuario', 'contrasenia'];
  dataSource: any = [];
  ngOnInit(): void {
    this.loadTeachers();
  }
  constructor(
    public docenteService: DocenteService
  ) {}
  loadTeachers() {
    return this.docenteService.GetDocentes().subscribe((data: any = []) =>
      this.dataSource =  new MatTableDataSource(data) );
  }

}
