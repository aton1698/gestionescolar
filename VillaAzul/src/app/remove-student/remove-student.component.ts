import {Component, Input, OnInit} from '@angular/core';
import {StudentSearchComponent} from '../student-search/student-search.component';
import {UserService} from '../services/user.service';
import {StudentService} from '../services/student.service';
import {MatTableDataSource} from '@angular/material/table';
import {StudentID} from '../model/student';

@Component({
  selector: 'app-remove-student',
  templateUrl: './remove-student.component.html',
  styleUrls: ['./remove-student.component.css']
})
export class RemoveStudentComponent implements OnInit {

  nombre: string;
  show = false;
  students: any = [];
  exist = true;

  displayedColumns: string[] = ['nombre', 'apellido1', 'apellido2', 'sexo', 'fecha_admision', 'accion'];

  @Input() test: StudentSearchComponent;

  constructor(public StudentService: StudentService, private userService: UserService) { }

  search(nombre) {
    if ( this.StudentService.GetStudentsFilter('nombre', nombre).subscribe((data: any = []) => this.students =  new MatTableDataSource(data))) {
      this.show = true;
    } else {
      this.exist = false;
    }
    // setTimeout(() => {  this.show = true; }, 3000);
  }

  removeFromGroup(element: StudentID) {
    if (element.id_grupo === this.userService.getUserLoggedIn().currentGroup ) {
      console.log(element);
      let test;
      console.log();
      test = new StudentID(element.nombre, element.id, element.apellido1, element.apellido2, element.sexo, element.fecha_admision, element.edad_febrero,
        element.fecha_nacimiento, element.centro_de_pertenencia, element.centro_traslado, element.repitencia, element.anios_escolaridad,
        -1);

      this.StudentService.UpdateStudent(element.id, test).subscribe(res => {console.log('estudiante dado de baja'); });
      alert( element.nombre + ' se ha eliminado de su grupo' );
      window.location.reload();
    } else {
      alert(element.nombre + ' no se encuentra en su grupo' );
      this.show = false;
    }
  }

  ngOnInit() {
  }

}
