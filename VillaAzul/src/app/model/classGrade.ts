export class ClassGrade {
  constructor(
    // tslint:disable-next-line:variable-name
    public nombre_materia: string,
    public nota: number,
    // tslint:disable-next-line:variable-name
    public id_estudiante: number,
    // tslint:disable-next-line:variable-name
    public id_grupo: number,
    public periodo: string
  ) { }
}
