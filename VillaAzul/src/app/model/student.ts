export class Student {
  constructor(
    public nombre: string,
    public apellido1: string,
    public apellido2: string,
    public sexo: string,
    public fecha_admision: string,
    public edad_febrero: number,
    public fecha_nacimiento: string,
    public centro_de_pertenencia: string,
    public centro_traslado: string,
    public repitencia: boolean,
    public anios_escolaridad: number,
    public id_grupo: number
  ) { }
}

export class StudentID {
  constructor(
    public nombre: string,
    public  id: number,
    public apellido1: string,
    public apellido2: string,
    public sexo: string,
    public fecha_admision: string,
    public edad_febrero: number,
    public fecha_nacimiento: string,
    public centro_de_pertenencia: string,
    public centro_traslado: string,
    public repitencia: boolean,
    public anios_escolaridad: number,
    public id_grupo: number
  ) { }
}

