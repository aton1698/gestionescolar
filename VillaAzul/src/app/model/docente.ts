export class Docente {
  constructor(
    public nombre: string,
    public apellido1: string,
    public apellido2: string,
    public usuario: string,
    public contrasenia: string
  ) { }
}

export class DocenteID {
  constructor(
    public id: number,
    public nombre: string,
    public apellido1: string,
    public apellido2: string,
    public usuario: string,
    public contrasenia: string
  ) { }
}
