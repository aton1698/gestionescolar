import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule, MatIconModule, MatTableModule, MatToolbarModule, MatCheckboxModule, MatNativeDateModule} from '@angular/material';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { GroupAdminComponent } from './group-admin/group-admin.component';
import { ReportsComponent } from './reports/reports.component';
import { GroupTableComponent } from './group-table/group-table.component';
import { HttpClientModule } from '@angular/common/http';
import { AttendanceComponent } from './attendance/attendance.component';
import { AttendanceTableComponent } from './attendance-table/attendance-table.component';
import { DocenteService } from './services/docente.service';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { StudentFormComponent } from './student-form/student-form.component';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { GradeComponent } from './grade/grade.component';
import {MatExpansionModule} from '@angular/material/expansion';
import { GradeTableComponent } from './grade-table/grade-table.component';
import {MatSelectModule} from '@angular/material/select';
import {MatListModule} from '@angular/material/list';
import {StudentService} from './services/student.service';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { TeachersComponent } from './teachers/teachers.component';
import { TeachersTableComponent } from './teachers-table/teachers-table.component';
import { TeachersFormComponent } from './teachers-form/teachers-form.component';
import { DeleteStudentComponent } from './delete-student/delete-student.component';
import { DeleteTeacherComponent } from './delete-teacher/delete-teacher.component';
import { UpdateTeacherComponent } from './update-teacher/update-teacher.component';
import { UpdateStudentComponent } from './update-student/update-student.component';
import { CoursesComponent } from './courses/courses.component';
import { CoursesTableComponent } from './courses-table/courses-table.component';
import {AddStudentComponent} from './add-student/add-student.component';
import { AdminStudentsComponent } from './admin-students/admin-students.component';
import { GeneralStudentsTableComponent } from './general-students-table/general-students-table.component';
import { CheckStudentComponent } from './check-student/check-student.component';
import { DeleteCourseComponent } from './delete-course/delete-course.component';
import {groupModelService} from './services/group.service';
import { Html2pdfComponent } from './html2pdf/html2pdf.component';
import { SpecificGroupTableComponent } from './specific-group-table/specific-group-table.component';
import {ClassGradeService} from './services/class-grade.service';
import {AssistanceService} from './services/assistance.service';
import { StudentSearchComponent } from './student-search/student-search.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {ofertaCursosService} from './services/ofertaCursosService';
import {RemoveStudentComponent} from './remove-student/remove-student.component';
import { MonthlyAssistanceComponent } from './monthly-assistance/monthly-assistance.component';
import { AnnualAssistanceComponent } from './annual-assistance/annual-assistance.component';
import { AnnualGradeComponent } from './annual-grade/annual-grade.component';
import {ToastrModule} from 'ngx-toastr';
import { TeacherAssignmentComponent } from './teacher-assignment/teacher-assignment.component';
import { AddCourseComponent } from './add-course/add-course.component';
import { AddingCourseComponent } from './adding-course/adding-course.component';

@NgModule({
    declarations: [
        AppComponent,
        ToolbarComponent,
        HomeComponent,
        GroupAdminComponent,
        ReportsComponent,
        GroupTableComponent,
        StudentFormComponent,
        LoginComponent,
        AttendanceComponent,
        AttendanceTableComponent,
        StudentFormComponent,
        GradeComponent,
        GradeTableComponent,
        AdminHomeComponent,
        TeachersComponent,
        TeachersTableComponent,
        TeachersFormComponent,
        DeleteStudentComponent,
        DeleteTeacherComponent,
        UpdateTeacherComponent,
        UpdateStudentComponent,
        CoursesComponent,
        CoursesTableComponent,
        AddStudentComponent,
        AdminStudentsComponent,
        GeneralStudentsTableComponent,
        CheckStudentComponent,
        AddCourseComponent,
        DeleteCourseComponent,
        Html2pdfComponent,
        SpecificGroupTableComponent,
        StudentSearchComponent,
        RemoveStudentComponent,
        MonthlyAssistanceComponent,
        AnnualAssistanceComponent,
        AnnualGradeComponent,
        TeacherAssignmentComponent,
        AddingCourseComponent
    ],
    imports: [
        AppRoutingModule,
        BrowserModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        MatIconModule,
        MatButtonModule,
        MatTableModule,
        HttpClientModule,
        MatCheckboxModule,
        HttpClientModule,
        MatFormFieldModule,
        MatDatepickerModule,
        FormsModule,
        MatTabsModule,
        MatInputModule,
        MatCardModule,
        MatExpansionModule,
        MatSelectModule,
        MatListModule,
        MatNativeDateModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        ToastrModule.forRoot()
    ],
  providers: [DocenteService, StudentService, groupModelService, MatDatepickerModule, ClassGradeService, AssistanceService, ofertaCursosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
