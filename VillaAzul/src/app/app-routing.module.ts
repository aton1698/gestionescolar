import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GroupAdminComponent} from './group-admin/group-admin.component';
import {ReportsComponent} from './reports/reports.component';
import {HomeComponent} from './home/home.component';
import {LoginComponent } from './login/login.component';
import { AttendanceComponent } from './attendance/attendance.component';
import { GradeComponent } from './grade/grade.component';
import {AdminHomeComponent} from './admin-home/admin-home.component';
import {TeachersComponent} from './teachers/teachers.component';
import {CoursesComponent} from './courses/courses.component';
import {AdminStudentsComponent} from './admin-students/admin-students.component';
import {Html2pdfComponent} from './html2pdf/html2pdf.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'groupAdmin', component: GroupAdminComponent},
  {path: 'reports', component: ReportsComponent},
  {path: 'login', component: LoginComponent, pathMatch: 'full'},
  {path: 'attendance', component: AttendanceComponent},
  {path: 'grade', component: GradeComponent},
  {path: 'admin', component: AdminHomeComponent},
  {path: 'teachers', component: TeachersComponent },
  {path: 'courses', component: CoursesComponent },
  {path: 'adminStudents', component: AdminStudentsComponent},
  {path: 'pdfConverter', component: Html2pdfComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
