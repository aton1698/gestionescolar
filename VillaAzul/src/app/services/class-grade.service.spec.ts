import { TestBed } from '@angular/core/testing';

import { ClassGradeService } from './class-grade.service';

describe('ClassGradeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClassGradeService = TestBed.get(ClassGradeService);
    expect(service).toBeTruthy();
  });
});
