import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {ofertaCursos} from '../model/ofertaCursos';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})

// tslint:disable-next-line:class-name
export class ofertaCursosService {
  // Base url
  baseurl = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // POST
  // This function receives the information for the ofertaCursos and creates the http post request for the API
  // TODO Figure out how does this receives the data, is it a ofertaCursos object?
  CreateofertaCurso(data): Observable<ofertaCursos> {
    console.log(this.baseurl + '/api/oferta_cursos', JSON.stringify(data));
    return this.http.post<ofertaCursos>(this.baseurl + '/api/oferta_cursos', JSON.stringify(data), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  // GET
  // Function that receives a specific ID and returns the ofertaCursos for that ID
  GetofertaCurso(id): Observable<ofertaCursos> {
    console.log(this.baseurl + '/api/oferta_cursos/' + id);
    return this.http.get<ofertaCursos>(this.baseurl + '/api/oferta_cursos/' + id)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }
  // GET
  // Function that returns all the ofertaCursoss in the database
  GetofertaCursos(): Observable<ofertaCursos> {
    console.log(this.baseurl + '/api/oferta_cursos');
    return this.http.get<ofertaCursos>(this.baseurl + '/api/oferta_cursos')
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }
  GetofertaCursosByName(name): Observable<ofertaCursos> {
    console.log(this.baseurl + '/api/oferta_cursos?filter=%7B%22where%22%3A%7B%22nombre_curso%22%3A%22' + name + '%22%7D%7D');
    return this.http.get<ofertaCursos>(this.baseurl + '/api/oferta_cursos')
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }


  // PUT
  UpdateofertaCurso(id, data): Observable<ofertaCursos> {
    console.log(this.baseurl + '/api/oferta_cursos' + id, JSON.stringify(data));
    return this.http.put<ofertaCursos>(this.baseurl + '/api/oferta_cursos' + id, JSON.stringify(data), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  // DELETE
  // Function that receives an ID and deletes the ofertaCursos with that ID
  DeleteofertaCurso(id) {
    console.log(this.baseurl + '/api/oferta_cursos/' + id);
    return this.http.delete<ofertaCursos>(this.baseurl + '/api/oferta_cursos/' + id, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  // Error handling
  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
