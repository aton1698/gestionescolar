import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ClassGrade } from '../model/classGrade';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import {groupModel} from '../model/groupModel';


@Injectable({
  providedIn: 'root'
})

export class ClassGradeService {
  // Base url
  baseurl = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // POST
  // This function receives the information for the ClassGrade and creates the http post request for the API
  // TODO Figure out how does this receives the data, is it a ClassGrade object?
  CreateClassGrade(data): Observable<ClassGrade> {
    console.log(this.baseurl + '/api/materia', JSON.stringify(data));
    return this.http.post<ClassGrade>(this.baseurl + '/api/materia', JSON.stringify(data), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  // GET
  // Function that receives a specific ID and returns the ClassGrade for that ID
  GetClassGrade(id): Observable<ClassGrade> {
    console.log(this.baseurl + '/api/materia/' + id);
    return this.http.get<ClassGrade>(this.baseurl + '/api/materia/' + id)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  // GET
  // Function that receives a specific ID and returns the ClassGrade for that ID
  GetSpecificClassGrade(groupId): Observable<ClassGrade> {
    console.log(this.baseurl + '/api/materia?filter=%7B%22where%22%3A%7B%22and%22%3A%20%5B%7B%22id_grupo%22%3A%20' + groupId + '%7D%5D%20%7D%20%7D');
    return this.http.get<ClassGrade>(this.baseurl + '/api/materia?filter=%7B%22where%22%3A%7B%22and%22%3A%20%5B%7B%22id_grupo%22%3A%20' + groupId + '%7D%5D%20%7D%20%7D')
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  // GET
  // Function that returns all the ClassGrades in the database
  GetClassGrades(): Observable<ClassGrade> {
    console.log(this.baseurl + '/api/materia');
    return this.http.get<ClassGrade>(this.baseurl + '/api/materia')
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  // GET
  // Function that returns all the grades that belong to a specific group
  GetGradesFromGroup(groupId): Observable<ClassGrade> {
    console.log('/api/grupos/' + groupId + '/materias');
    return this.http.get<ClassGrade>(this.baseurl + '/api/grupos/' + groupId + '/materias')
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }


  // PUT
  UpdateClassGrade(id, data): Observable<ClassGrade> {
    console.log(this.baseurl + '/api/materia/' + id, JSON.stringify(data));
    return this.http.put<ClassGrade>(this.baseurl + '/api/materia/' + id, JSON.stringify(data), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  // DELETE
  // Function that receives an ID and deletes the ClassGrade with that ID
  DeleteClassGrade(id) {
    console.log(this.baseurl + '/api/materia/' + id);
    return this.http.delete<ClassGrade>(this.baseurl + '/api/materia/' + id, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  // Error handling
  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
