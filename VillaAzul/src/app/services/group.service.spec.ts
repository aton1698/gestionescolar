import { TestBed } from '@angular/core/testing';

import { groupModelService } from './group.service';

describe('GroupService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: groupModelService = TestBed.get(groupModelService);
    expect(service).toBeTruthy();
  });
});
