import { TestBed } from '@angular/core/testing';

import { OfertaCursosService } from './ofertaCursosService';

describe('OfertaCursosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OfertaCursosService = TestBed.get(OfertaCursosService);
    expect(service).toBeTruthy();
  });
});
