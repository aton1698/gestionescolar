import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Docente } from '../model/docente';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})

export class DocenteService {
  // Base url
  baseurl = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // POST
  // This function receives the information for the docente and creates the http post request for the API
  // TODO Figure out how does this receives the data, is it a docente object?
  CreateDocente(data): Observable<Docente> {
    console.log('/api/docentes' + JSON.stringify(data));
    return this.http.post<Docente>(this.baseurl + '/api/docentes', JSON.stringify(data), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  // GET
  // Function that receives a specific ID and returns the docente for that ID
  GetDocente(id): Observable<Docente> {
    console.log('/api/docentes/' + id);
    return this.http.get<Docente>(this.baseurl + '/api/docentes/' + id)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  // GET
  // Function that receives a specific ID and returns the ID info for the courses that teacher has
  GetClassesDocente(docenteId): Observable<Docente> {
    console.log(this.baseurl + '/api/docentes/' + docenteId + '/gruposDocente');
    return this.http.get<Docente>(this.baseurl + '/api/docentes/' + docenteId + '/gruposDocente')
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }



  // GET
  // Function that returns all the docentes in the database
  GetDocentes(): Observable<Docente> {
    console.log(this.baseurl + '/api/docentes');
    return this.http.get<Docente>(this.baseurl + '/api/docentes')
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  // GET
  // Function that returns a specific docente based upon its username
  GetDocentesFindOne(username): Observable<Docente> {
    console.log('/api/docentes/findOne?filter=%7B%22where%22%3A%7B%22usuario%22%3A%22' + username + '%22%7D%7D');

    return this.http.get<Docente>(this.baseurl + '/api/docentes/findOne?filter=%7B%22where%22%3A%7B%22usuario%22%3A%22' + username + '%22%7D%7D')
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  // PUT
  UpdateDocente(id, data): Observable<Docente> {
    console.log(this.baseurl + '/api/docentes/' + id, JSON.stringify(data), this.httpOptions);
    return this.http.put<Docente>(this.baseurl + '/api/docentes/' + id, JSON.stringify(data), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  // DELETE
  // Function that receives an ID and deletes the docente with that ID
  DeleteDocente(id) {
    console.log(this.baseurl + '/api/docentes/' + id);
    return this.http.delete<Docente>(this.baseurl + '/api/docentes/' + id, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  // Error handling
  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
