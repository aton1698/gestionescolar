import { Component, OnInit } from '@angular/core';
import {ofertaCursosService} from "../services/ofertaCursosService";
import {ofertaCursos} from "../model/ofertaCursos";

@Component({
  selector: 'app-adding-course',
  templateUrl: './adding-course.component.html',
  styleUrls: ['./adding-course.component.css']
})
export class AddingCourseComponent implements OnInit {
  insertado = false;

  constructor(public OfertaCursoService: ofertaCursosService) { }

  insert(nombre) {
    let test;
    test = new ofertaCursos(nombre);
    this.OfertaCursoService.CreateofertaCurso(test).subscribe(res => {console.log('Curso agregado'); });
    alert('Se ha agregado con éxito el curso');
    window.location.reload();
  }

  ngOnInit() {
  }

}
