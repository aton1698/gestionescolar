import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddingCourseComponent } from './adding-course.component';

describe('AddingCourseComponent', () => {
  let component: AddingCourseComponent;
  let fixture: ComponentFixture<AddingCourseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddingCourseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddingCourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
