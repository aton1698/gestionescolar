import { Component, OnInit } from '@angular/core';
import * as jspdf from 'jspdf';
import {UserService} from '../services/user.service';
import {StudentService} from '../services/student.service';
import {ClassGradeService} from '../services/class-grade.service';
import {ofertaCursosService} from '../services/ofertaCursosService';


@Component({
  selector: 'app-annual-grade',
  templateUrl: './annual-grade.component.html',
  styleUrls: ['./annual-grade.component.css']
})
export class AnnualGradeComponent implements OnInit {
  groupId: any;
  year = new Date().getFullYear();
  group;
  students: any = [];
  grades: any = [];
  loading = false;
  finalData: any = [];
  courses: any = [];
  header: any = [];
  body: any = [];
  // tslint:disable-next-line:no-shadowed-variable
  constructor(private userService: UserService,  public ofertaCursosService: ofertaCursosService, public classGradeService: ClassGradeService, public studentService: StudentService, ) { }
  ngOnInit() {
    this.group = this.userService.getUserLoggedIn().currentGroupName;
    this.groupId = this.userService.getUserLoggedIn().currentGroup;
    this.header = [[this.group + ' ' + new Date().getFullYear()]];
  }
  log() {
    this.loading = true;

    // const month = new Date().getMonth() + 1;
    this.studentService.GetStudentsFromGroup(this.groupId).subscribe((data: any = []) => this.students =  data);
    this.ofertaCursosService.GetofertaCursos().subscribe((data: any = []) => this.courses = data);
    this.classGradeService.GetSpecificClassGrade(this.groupId).subscribe((data: any = []) => this.grades =  data);

    setTimeout(() => {  this.completeData(); this.createHeader(); console.log(this.grades); }, 4000);
    setTimeout(() => {  this.generar(); }, 8000);
  }
  createHeader() {
    for (const index in this.courses) {
      this.header.push(this.courses[index].nombre_curso);
    }
  }

  completeData() {
    // tslint:disable-next-line:forin
    for (const index in this.students) {
      console.log('En for, con el estudiante: ' + this.students[index].id);
      this.calculateGrade(this.students[index].id);
    }
  }


  calculateGrade(studentID) {

    let primer: any = [];
    let segundo: any = [];
    let tercero: any = [];
    let currentCourse: any = [];

    const currentStudent = [studentID];
    const currentGradeStudent = this.filterStudent(studentID);
    console.log('TEST: ' + currentGradeStudent);
    for (const index in this.courses) {
      currentCourse = this.filterCourse(this.courses[index].nombre_curso, currentGradeStudent);


      primer = this.filterPeriod( 'I Trimestre', currentCourse);
      segundo = this.filterPeriod('II Trimestre', currentCourse);
      tercero = this.filterPeriod('III Trimestre', currentCourse);
      console.log('AJASASDA: ' + primer.nombre_materia);

      console.log('Primero: ' + primer.nota + ' Segundo:  '  + segundo.nota + ' Tercero ' + tercero.nota);
      currentStudent.push((primer.nota * 0.30) + (segundo.nota * 0.30) + (tercero.nota * 0.40));
    }
    this.finalData.push(currentStudent);
  }


  filterPeriod(period, arreglo) {
    let result: any;

    for (const index in arreglo) {
      if (arreglo[index].periodo === period) {
        result = arreglo[index];
      }
    }
    return result;
  }

  filterCourse(course, arreglo) {
    const result = [];
    for (const index in arreglo) {
      if (arreglo[index].nombre_materia === course) {
        console.log('Pushing: ' + arreglo[index].nombre_materia );
        result.push(arreglo[index]);
      }
    }
    return result;
  }

  filterStudent(student) {
    const result = [];
    for (const index in this.grades) {
      if (this.grades[index].id_estudiante === student) {
        console.log('Se encontró nota de ' + this.grades[index].id_estudiante);
        result.push(this.grades[index]);
      }
    }
    return result;
  }

  generar() {
    console.log('head: ' + this.header + ' body: ' + this.finalData);
    const doc = new jspdf();
    // It can parse html:
    // doc.autoTable({ html: '#my-table' });
    console.log('hola');
    // Or use javascript directly:
    doc.autoTable({
      //styles: { pageBreak: 'always' },
      //margin: { top: 10 },

      head: [this.header],
      body: this.finalData
    });
    doc.save('Notas anual ' + this.group + '.pdf');
    this.loading = false;

    // Se limpian todos los arreglos
    this.students = [];
    this.grades = [];
    this.finalData = [];
    this.courses = [];
    this.header = [];
    this.body = [];
    alert('Reporte generado con éxito, presione aceptar para iniciar la descarga');


  }
}
