import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnualGradeComponent } from './annual-grade.component';

describe('AnnualGradeComponent', () => {
  let component: AnnualGradeComponent;
  let fixture: ComponentFixture<AnnualGradeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnualGradeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnualGradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
