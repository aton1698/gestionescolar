import { Component, OnInit } from '@angular/core';
import {Period} from "../grade-table/grade-table.component";
import {UserService} from "../services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-grade',
  templateUrl: './grade.component.html',
  styleUrls: ['./grade.component.css']
})
export class GradeComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) {
    if (!this.userService.getUserLoggedIn() || this.userService.getUserLoggedIn().username === 'admin') {
      this.router.navigateByUrl('/login');
    }
  }
  year = new Date().getFullYear();
  group;
  ngOnInit() {
    this.group = this.userService.getUserLoggedIn().currentGroupName;
  }

}
