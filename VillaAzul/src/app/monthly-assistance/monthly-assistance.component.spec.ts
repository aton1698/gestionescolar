import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyAssistanceComponent } from './monthly-assistance.component';

describe('MonthlyAssistanceComponent', () => {
  let component: MonthlyAssistanceComponent;
  let fixture: ComponentFixture<MonthlyAssistanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonthlyAssistanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlyAssistanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
