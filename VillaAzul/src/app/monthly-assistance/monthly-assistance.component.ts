import { Component, OnInit } from '@angular/core';
import {Student} from '../model/student';
import {UserService} from '../services/user.service';
import {AssistanceService} from '../services/assistance.service';
import {StudentService} from '../services/student.service';
import {Assistance} from '../model/assistance';
import * as jspdf from 'jspdf';


export interface Months {
  value: number;
  viewValue: string;
}

@Component({
  selector: 'app-monthly-assistance',
  templateUrl: './monthly-assistance.component.html',
  styleUrls: ['./monthly-assistance.component.css']
})

export class MonthlyAssistanceComponent implements OnInit {

  months: Months[] = [
    {value: 1, viewValue: 'Enero'},
    {value: 2, viewValue: 'Febrero'},
    {value: 3, viewValue: 'Marzo'},
    {value: 4, viewValue: 'Abril'},
    {value: 5, viewValue: 'Mayo'},
    {value: 6, viewValue: 'Junio'},
    {value: 7, viewValue: 'Julio'},
    {value: 8, viewValue: 'Agosto'},
    {value: 9, viewValue: 'Setiembre'},
    {value: 10, viewValue: 'Octubre'},
    {value: 11, viewValue: 'Noviembre'},
    {value: 12, viewValue: 'Diciembre'},
  ];

  // This is just an example, the data has to be getted from the ts that is going to manage the session information
  year = new Date().getFullYear();
  group;
  totalAusencias = 0;
  ausenciasJustificadasH = 0;
  ausenciasJustificadasM = 0;
  ausenciasInjustificadasH = 0;
  ausenciasInjustificadasM = 0;

  totalTardias = 0;
  tardiasJustificadasH = 0;
  tardiasJustificadasM = 0;
  tardiasInjustificadasH = 0;
  tardiasInjustificadasM = 0;
  dataSource: any = [];
  students: Student;
  mes: any;
  show = false;
  loading = false;

  constructor(private userService: UserService, public assistanceService: AssistanceService, public studentService: StudentService) {

  }

  generar() {
    const doc = new jspdf();
    // It can parse html:
    // doc.autoTable({ html: '#my-table' });
    console.log('hola');
    // Or use javascript directly:
    doc.autoTable({
      head: [[this.mes.viewValue + ' ' + this.group, 'Total', 'H', 'M']],
      body: [
        ['Ausencias Motivadas', this.ausenciasJustificadasM + this.ausenciasJustificadasH, this.ausenciasJustificadasH, this.ausenciasJustificadasM],
        ['Ausencias Inmotivadas', this.ausenciasInjustificadasM + this.ausenciasInjustificadasH, this.ausenciasInjustificadasH, this.ausenciasJustificadasM],
        ['Total Ausencias', this.totalAusencias, this.ausenciasJustificadasH + this.ausenciasInjustificadasH, this.ausenciasJustificadasM + this.ausenciasInjustificadasM ],
        ['Tardías Motivadas', this.tardiasJustificadasM + this.tardiasJustificadasH, this.tardiasJustificadasH, this.tardiasJustificadasM],
        ['Tardías Inmotivadas', this.tardiasInjustificadasM + this.tardiasInjustificadasH, this.tardiasInjustificadasH, this.tardiasJustificadasM],
        ['Total Tardías Motivadas', this.totalTardias, this.tardiasJustificadasH + this.tardiasInjustificadasH, this.tardiasJustificadasM + this.tardiasInjustificadasM],
      ],
    });

    doc.save('Asistencia mensual ' + this.group + '.pdf');
    this.loading = false;
    this.resetValues();
    alert('Reporte generado con éxito, presione aceptar para iniciar la descarga');
  }
  log() {
    this.loading = true;
    this.show = false;
    console.log(this.mes);
    // const month = new Date().getMonth() + 1;
    this.assistanceService.GetAssistanceFilters(this.userService.getUserLoggedIn().currentGroup, 'mes', this.mes.value).subscribe((data: any = []) =>
      this.dataSource =  data);
    setTimeout(() => {  this.completeData(); }, 3000);
    setTimeout(() => {  this.generar(); }, 9000);
  }
  completeData() {
    // tslint:disable-next-line:forin
    for (const index in this.dataSource) {
      setTimeout(() => { this.defineData(this.dataSource[index]); console.log('En for, con el estudiante: ' + this.dataSource[index].id_estudiante); }, 3000);
    }
  }
  defineData(currentAssitance: Assistance) {
    this.studentService.GetStudent(currentAssitance.id_estudiante).subscribe((data: Student) =>
      this.students =  data);
    setTimeout(() => {
      if (this.students.sexo === ' Femenino') {
        if (currentAssitance.tardia_injustificada) {
          this.tardiasInjustificadasM += 1;
          this.totalTardias += 1;
        } else if (currentAssitance.tardia_justificada) {
          this.tardiasJustificadasM += 1;
          this.totalTardias += 1;
        } else if (currentAssitance.ausencia_injustificada) {
          this.ausenciasInjustificadasM += 1;
          this.totalAusencias += 1;
        } else if (currentAssitance.ausencia_justificada) {
          this.ausenciasJustificadasM += 1;
          this.totalAusencias += 1;
        }
      } else {
        if (currentAssitance.tardia_injustificada) {
          this.tardiasInjustificadasH += 1;
          this.totalTardias += 1;
        } else if (currentAssitance.tardia_justificada) {
          this.tardiasJustificadasH += 1;
          this.totalTardias += 1;
        } else if (currentAssitance.ausencia_injustificada) {
          this.ausenciasInjustificadasH += 1;
          this.totalAusencias += 1;
        } else if (currentAssitance.ausencia_justificada) {
          this.ausenciasJustificadasH += 1;
          this.totalAusencias += 1;
        }
      } }, 100);
  }
  ngOnInit() {
    this.group = this.userService.getUserLoggedIn().currentGroupName;
  }

  next() {
    this.show = true;
  }
  resetValues() {
    this.dataSource = [];
    this.totalAusencias = 0;
    this.ausenciasJustificadasH = 0;
    this.ausenciasJustificadasM = 0;
    this.ausenciasInjustificadasH = 0;
    this.ausenciasInjustificadasM = 0;

    this.totalTardias = 0;
    this.tardiasJustificadasH = 0;
    this.tardiasJustificadasM = 0;
    this.tardiasInjustificadasH = 0;
    this.tardiasInjustificadasM = 0;
  }
}
