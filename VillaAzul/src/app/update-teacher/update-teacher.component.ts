import { Component, OnInit } from '@angular/core';
import {DocenteService} from '../services/docente.service';
import {DocenteID} from '../model/docente';

export interface UpdateField {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-update-teacher',
  templateUrl: './update-teacher.component.html',
  styleUrls: ['./update-teacher.component.css']
})
export class UpdateTeacherComponent implements OnInit {
  docente: DocenteID;

  status: UpdateField[] = [
    {value: 'usuario', viewValue: 'Usuario'},
    {value: 'contrasenia', viewValue: 'Contraseña'},
  ];
  campo: string;

  constructor(public docenteService: DocenteService) { }

  ngOnInit() {
  }
  updateTeacher(usuario, valor) {
    if(this.docenteService.GetDocentesFindOne(usuario).subscribe((data: DocenteID) => {
      this.docente = data;
    })){
      setTimeout(() => {  this.updating(valor); }, 1000);
    } else {
      alert('Ha ocurrido un error');
    }
  }
  updating(valor) {
    if (this.campo === 'contrasenia' ) {
      const newValues = new DocenteID(this.docente.id, this.docente.nombre, this.docente.apellido1, this.docente.apellido2, this.docente.usuario, valor);
      if (this.docenteService.UpdateDocente(this.docente.id, newValues).subscribe(res => {console.log('Docente actulizado'); })) {
        alert('Se ha actulizado la contraseña de ' + this.docente.nombre + ' de manera correcta');
        window.location.reload();
      } else {
        console.log('ERROR ACTUALIZANDO');
      }
    } else {
      const newValues = new DocenteID(this.docente.id, this.docente.nombre, this.docente.apellido1, this.docente.apellido2, valor, this.docente.contrasenia);
      if (this.docenteService.UpdateDocente(this.docente.id, newValues).subscribe(res => {console.log('Docente actualizado'); })) {
        alert('Se ha actulizado el usuario de ' + this.docente.nombre + ' de manera correcta');
        window.location.reload();
      } else {
        console.log('ERROR ACTUALIZANDO');
      }
    }
  }
}
