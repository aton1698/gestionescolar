import { Component, OnInit, ElementRef , ViewChild} from '@angular/core';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import 'src/jspdf.plugin.autotable.min.js';

@Component({
  selector: 'app-html2pdf',
  templateUrl: './html2pdf.component.html',
  styleUrls: ['./html2pdf.component.css']
})
export class Html2pdfComponent {
  generar() {
    const doc = new jspdf();
    // It can parse html:
    doc.autoTable({ html: '#my-table' });
    console.log('hola');
    // Or use javascript directly:
    doc.autoTable({
      head: [['Name', 'Email', 'Country']],
      body: [
        ['David', 'david@example.com', 'Sweden'],
        ['Castille', 'castille@example.com', 'Norway'],
        // ...
      ],
    });

    doc.save('table.pdf');
  }
}
