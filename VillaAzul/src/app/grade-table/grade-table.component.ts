import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {StudentService} from '../services/student.service';
import {ofertaCursosService} from '../services/ofertaCursosService';
import {UserService} from '../services/user.service';
import {ClassGrade} from '../model/classGrade';
import {ClassGradeService} from '../services/class-grade.service';
import {StudentID} from '../model/student';
import {ToastrService} from 'ngx-toastr';

export interface Food {
  value: string;
  viewValue: string;
}

export interface Period {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-grade-table',
  templateUrl: './grade-table.component.html',
  styleUrls: ['./grade-table.component.css']
})
export class GradeTableComponent implements OnInit {
  panelOpenState = false;
  periods: Period[] = [
    {value: 'I Trimestre', viewValue: 'I Trimestre'},
    {value: 'II Trimestre', viewValue: 'II Trimestre'},
    {value: 'III Trimestre', viewValue: 'III Trimeste'}
  ];
  periodo = 'I Trimestre';
  curso = 'Español';

  dataSource: any = [];
  dataCouses: any = [];


  constructor(
    public studentService: StudentService, private userService: UserService, public OfertaCursoService: ofertaCursosService,
    public ClassGradeService: ClassGradeService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.loadStudents();
    this.loadCourses();
  }
  loadStudents() {
    return this.studentService.GetStudentsFromGroup(this.userService.getUserLoggedIn().currentGroup).subscribe((data: any = []) =>
      this.dataSource =  data);
  }
  loadCourses() {
    return this.OfertaCursoService.GetofertaCursos().subscribe((data: any = []) =>
      this.dataCouses =  data);
  }
  insert(estudiante: StudentID, nota) {
    let test;
    if(nota > 100 || nota < 0){
      alert('La nota ingresada no es válida, el valor debe estar entre 0 y 100');
      return;
    }
    test = new ClassGrade(this.curso, nota,  estudiante.id, this.userService.getUserLoggedIn().currentGroup, this.periodo);
    this.ClassGradeService.CreateClassGrade(test).subscribe(res => {this.showToaster(); });
  }
  showToaster() {
    this.toastr.success('Se ha ingresado la nota de manera correcta', '', {
      timeOut: 2000
    });
  }
}
