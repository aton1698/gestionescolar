import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {StudentService} from '../services/student.service';


@Component({
  selector: 'app-group-table',
  templateUrl: './group-table.component.html',
  styleUrls: ['./group-table.component.css']
})
export class GroupTableComponent implements OnInit{
  // displayedColumns: string[] = ['id', 'nombre', 'apellido1', 'apellido2', 'sexo', 'fecha_admision', 'edad_febrero', 'fecha_nacimiento', 'centro_de_pertenencia', 'centro_traslado',
    // 'repitencia', 'anios_escolaridad'];

  displayedColumns: string[] = ['nombre', 'apellido1', 'apellido2', 'sexo', 'fecha_admision', 'edad_febrero', 'fecha_nacimiento', 'centro_de_pertenencia', 'centro_traslado', 'repitencia', 'anios_escolaridad'];
  dataSource: any = [];
  ngOnInit(): void {
    this.loadStudents();
  }
  constructor(
    public StudenteService: StudentService
  ) {}
  loadStudents() {
    return this.StudenteService.GetStudents().subscribe((data: any = []) =>
      this.dataSource =  new MatTableDataSource(data));
  }
  // dataSource = ELEMENT_DATA;
}
