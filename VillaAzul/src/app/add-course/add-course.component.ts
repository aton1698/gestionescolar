import { Component, OnInit } from '@angular/core';
import {ofertaCursos, ofertaCursosID} from '../model/ofertaCursos';
import {ofertaCursosService} from '../services/ofertaCursosService';

@Component({
  selector: '.' +
    'app-add-course',
  templateUrl: './add-course.component.html',
  styleUrls: ['./add-course.component.css']
})
export class AddCourseComponent implements OnInit {
  insertado = false;

  constructor(public OfertaCursoService: ofertaCursosService) { }

  insert(nombre) {
    let test;
    test = new ofertaCursos(nombre);
    this.OfertaCursoService.CreateofertaCurso(test).subscribe(res => {console.log('Curso agregado'); });
    alert('Se ha agregado con éxito el curso');
  }

  ngOnInit() {
  }

}
