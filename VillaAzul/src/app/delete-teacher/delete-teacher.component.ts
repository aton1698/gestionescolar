import { Component, OnInit } from '@angular/core';
import {DocenteID} from '../model/docente';
import {DocenteService} from '../services/docente.service';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-delete-teacher',
  templateUrl: './delete-teacher.component.html',
  styleUrls: ['./delete-teacher.component.css']
})
export class DeleteTeacherComponent implements OnInit {
  teacher: any = {};

  ngOnInit() {
  }
  constructor(public docenteService: DocenteService) { }

  search(usuario) {
    // @ts-ignore
    if (this.docenteService.GetDocentesFindOne(usuario).subscribe((data: {}) => {
      this.teacher = data; } )) {
      setTimeout(() => {  this.docenteService.DeleteDocente(this.teacher.id).subscribe(res => {console.log('Docente eliminado'); }); }, 1000);
      setTimeout(() => {  alert('Se ha eliminado el docente con éxito'); window.location.reload(); window.location.reload(); }, 2000);
    } else {
      alert('El usuario ingresado no éxiste en el sistema');
    }
  }
}
