import { Component, OnInit } from '@angular/core';
import {Docente} from '../model/docente';
import {DocenteService} from '../services/docente.service';
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-teachers-form',
  templateUrl: './teachers-form.component.html',
  styleUrls: ['./teachers-form.component.css']
})
export class TeachersFormComponent {


  constructor(public docenteService: DocenteService) {
  }


  log(x) {console.log(x); }
  createTeacher(nombre, apellido1, apellido2, usuario, contrasenia) {
    const docenteModel = new Docente(nombre, apellido1, apellido2, usuario, contrasenia);
    console.log(docenteModel);
    if (this.docenteService.CreateDocente(docenteModel).subscribe(res => {console.log('Docente agregado'); })) {
      alert( nombre + ' ha sido agregado' );
      window.location.reload();
    } else {
      alert( 'Se ha presentado un error al intentar registar al docente' );
    }
  }

}
