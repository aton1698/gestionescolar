import {Component, HostListener, Input, OnInit} from '@angular/core';
import {StudentSearchComponent} from '../student-search/student-search.component';
import {StudentService} from '../services/student.service';
import {MatTableDataSource} from '@angular/material/table';
import {Student, StudentID} from '../model/student';
import {UserService} from '../services/user.service';
import {group} from '@angular/animations';
import {Router} from '@angular/router';
import {timeout} from "rxjs/operators";

@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.css']
})
export class AddStudentComponent implements OnInit {

  nombre: string;
  show = false;
  students: any = [];
  exist = true;

  displayedColumns: string[] = ['nombre', 'apellido1', 'apellido2', 'sexo', 'fecha_admision', 'accion'];


  @Input() test: StudentSearchComponent;

  // tslint:disable-next-line:no-shadowed-variable

  constructor(public StudentService: StudentService, private userService: UserService, private router: Router) { }
  search(nombre) {
    if ( this.StudentService.GetStudentsFilter('nombre', nombre).subscribe((data: any = []) => this.students =  new MatTableDataSource(data))) {
      this.show = true;
    } else {
      this.exist = false;
    }
    //setTimeout(() => {  this.show = true; }, 3000);
  }
  insert(element: StudentID) {
    if (element.id_grupo !== this.userService.getUserLoggedIn().currentGroup ){
      console.log(element);
      let test;
      console.log();
      test = new StudentID(element.nombre, element.id, element.apellido1, element.apellido2, element.sexo, element.fecha_admision, element.edad_febrero,
        element.fecha_nacimiento, element.centro_de_pertenencia, element.centro_traslado, element.repitencia, element.anios_escolaridad,
        this.userService.getUserLoggedIn().currentGroup);

      this.StudentService.UpdateStudent(element.id, test).subscribe(res => {console.log('estudiante agregado'); });
      alert( element.nombre + ' ha sido agregado' );
      window.location.reload();
    } else {
     alert(element.nombre + ' ya se encuentra en su grupo' );
    }
  }

  ngOnInit() {
  }

}
