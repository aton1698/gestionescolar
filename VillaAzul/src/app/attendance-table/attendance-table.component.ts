import { Component, OnInit } from '@angular/core';
import {MatTableDataSource } from '@angular/material/table';

import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {FormControl} from '@angular/forms';
import {Student, StudentID} from '../model/student';
import {Assistance} from '../model/assistance';
import {StudentService} from '../services/student.service';
import {UserService} from '../services/user.service';
import {AssistanceService} from '../services/assistance.service';

export interface Element {
  ausenciaInjustificada: boolean;
  ausenciaJustificada: boolean;
  tardiaInjustificada: boolean;
  tardiaJustificada: boolean;
  presente: boolean;
  name: string;
  day: string;
  month: string;
  year: string;
}

export interface Test {
  nombre: string;

}

@Component({
  selector: 'app-attendance-table',
  templateUrl: './attendance-table.component.html',
  styleUrls: ['./attendance-table.component.css'],
  providers: [{provide: MAT_DATE_LOCALE, useValue: 'fr-FR'}]
})
export class AttendanceTableComponent implements OnInit {
  assistanceModel = new Assistance(7,2,2,1,2020,false, false,false,false,true,"Masculino","Braulio");

  displayedColumns = ['nombre', 'ausenciaInjustificada', 'ausenciaJustificada', 'tardiaInjustificada', 'tardiaJustificada', 'presente'];

  dataSource: any = [];
  date = new FormControl(new Date());
  fecha = new Date();
  serializedDate = new FormControl((new Date()).toISOString());
  private element: StudentID;
  finalData: any = [];
  loading = true;
  notLoading = false;

  constructor( public StudenteService: StudentService, private userService: UserService, public AssistanceService: AssistanceService) { }

  ngOnInit() {
    this.loadStudents();
    setTimeout(() => {  this.makeObjects(); this.loading = false; this.notLoading = true; }, 3000);

  }
  loadStudents() {
    return this.StudenteService.GetStudentsFromGroup(this.userService.getUserLoggedIn().currentGroup) .subscribe((data: any = []) =>
      this.dataSource =  data);
  }
  insert() {
      // tslint:disable-next-line:forin
    for(var index in this.finalData) {
      console.log(this.finalData[index]);
      this.AssistanceService.CreateAssistance(this.finalData[index]).subscribe(res =>{console.log('Asistencia agregada'); });
      alert('Asistencia agregada');
    }
  }
  makeObjects() {
    // tslint:disable-next-line:forin
    for(var index in this.dataSource) {
      console.log(this.dataSource[index]);
      this.element = this.dataSource[index];
      this.finalData.push(
        new Assistance(this.element.id, this.userService.getUserLoggedIn().currentGroup, this.fecha.getDay(), this.fecha.getMonth() + 1, this.fecha.getFullYear(),
          false, false, false, false, false, this.element.sexo, this.element.nombre)
      );
      console.log(this.finalData[index]);
    }
  }

}
