import { Component, OnInit } from '@angular/core';
import {StudentService} from '../services/student.service';
import {MatTableDataSource} from '@angular/material/table';
import {ofertaCursosService} from '../services/ofertaCursosService';

@Component({
  selector: 'app-courses-table',
  templateUrl: './courses-table.component.html',
  styleUrls: ['./courses-table.component.css']
})
export class CoursesTableComponent implements OnInit {

  displayedColumns: string[] = ['nombre'];
  dataSource: any = [];
  ngOnInit(): void {
    this.loadStudents();
  }
  constructor(
    public OfertaCursoService: ofertaCursosService
  ) {}
  loadStudents() {
    return this.OfertaCursoService.GetofertaCursos().subscribe((data: any = []) =>
      this.dataSource =  new MatTableDataSource(data) );
  }

}
