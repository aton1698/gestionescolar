import { Component, OnInit } from '@angular/core';
import {UserService} from '../services/user.service';
import {Router} from '@angular/router';
import * as jspdf from 'jspdf';
import 'src/jspdf.plugin.autotable.min.js';
import {AssistanceService} from '../services/assistance.service';
import {Assistance} from '../model/assistance';
import {Student} from '../model/student';
import {StudentService} from '../services/student.service';


@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {
  year = new Date().getFullYear();
  group;
  constructor(private userService: UserService, private router: Router, public assistanceService: AssistanceService, public studentService: StudentService) {
    if (!this.userService.getUserLoggedIn() || this.userService.getUserLoggedIn().username === 'admin') {
      this.router.navigateByUrl('/login');
    }
  }

  ngOnInit(): void {
    this.group = this.userService.getUserLoggedIn().currentGroupName;
  }
}
