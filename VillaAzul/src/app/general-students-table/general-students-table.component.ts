import { Component, OnInit } from '@angular/core';
import {StudentService} from '../services/student.service';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-general-students-table',
  templateUrl: './general-students-table.component.html',
  styleUrls: ['./general-students-table.component.css']
})
export class GeneralStudentsTableComponent implements OnInit {

  displayedColumns: string[] = ['nombre', 'apellido1', 'apellido2', 'sexo', 'fecha_admision', 'edad_febrero', 'fecha_nacimiento', 'centro_de_pertenencia', 'centro_traslado', 'repitencia', 'anios_escolaridad'];
  dataSource: any = [];
  ngOnInit(): void {
    this.loadStudents();
  }
  constructor(
    public StudenteService: StudentService
  ) {}
  loadStudents() {
    return this.StudenteService.GetStudents().subscribe((data: any = []) =>
      this.dataSource =  new MatTableDataSource(data) );
  }
  // dataSource = ELEMENT_DATA;
}
