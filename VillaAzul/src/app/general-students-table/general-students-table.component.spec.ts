import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralStudentsTableComponent } from './general-students-table.component';

describe('GeneralStudentsTableComponent', () => {
  let component: GeneralStudentsTableComponent;
  let fixture: ComponentFixture<GeneralStudentsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralStudentsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralStudentsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
