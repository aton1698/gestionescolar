import { Component, OnInit } from '@angular/core';
import {UserService} from '../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-group-admin',
  templateUrl: './group-admin.component.html',
  styleUrls: ['./group-admin.component.css']
})
export class GroupAdminComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) {
    if (!this.userService.getUserLoggedIn() || this.userService.getUserLoggedIn().username === 'admin') {
      this.router.navigateByUrl('/login');
    }
  }
  year = new Date().getFullYear();
  group;

  ngOnInit() {
    this.group = this.userService.getUserLoggedIn().currentGroupName;
  }

}
