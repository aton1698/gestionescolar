import { Component, OnInit } from '@angular/core';
import {StudentService} from '../services/student.service';
import {MatTableDataSource} from "@angular/material/table";
import {UserService} from "../services/user.service";

@Component({
  selector: 'app-specific-group-table',
  templateUrl: './specific-group-table.component.html',
  styleUrls: ['./specific-group-table.component.css']
})
export class SpecificGroupTableComponent implements OnInit {
  displayedColumns: string[] = ['nombre', 'apellido1', 'apellido2', 'sexo', 'fecha_admision', 'edad_febrero', 'fecha_nacimiento', 'centro_de_pertenencia', 'centro_traslado', 'repitencia', 'anios_escolaridad'];
  dataSource: any = [];
  ngOnInit(): void {
    this.loadStudents();
  }
  constructor(
    public StudenteService: StudentService,
    private userService: UserService
  ) {}
  loadStudents() {
    return this.StudenteService.GetStudentsFromGroup(this.userService.getUserLoggedIn().currentGroup).subscribe((data: any = []) =>
      this.dataSource =  new MatTableDataSource(data));
  }
  // dataSource = ELEMENT_DATA;
}
