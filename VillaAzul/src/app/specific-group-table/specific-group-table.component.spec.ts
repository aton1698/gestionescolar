import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecificGroupTableComponent } from './specific-group-table.component';

describe('SpecificGroupTableComponent', () => {
  let component: SpecificGroupTableComponent;
  let fixture: ComponentFixture<SpecificGroupTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecificGroupTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecificGroupTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
