import { Component, OnInit } from '@angular/core';
import {UserService} from "../services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.css']
})
export class TeachersComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) {
    if (!this.userService.getUserLoggedIn() || this.userService.getUserLoggedIn().username !== 'admin') {
      this.router.navigateByUrl('/login');
    }
  }
  year = new Date().getFullYear();

  ngOnInit() {
  }

}
