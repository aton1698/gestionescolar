import { Component, OnInit } from '@angular/core';
import {Student} from '../model/student';
import { StudentService } from '../services/student.service';
import {Period} from '../grade-table/grade-table.component';

export interface Status {
  value: boolean;
  viewValue: string;
}

@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent {
  studentModel = new Student('', '', '', 'Masculino', Date(), 0, '2020-01-05T00:45:20.868Z', 'NULL', 'NULL', false, 0, -1);
  status: Status[] = [
    {value: true, viewValue: 'Sí'},
    {value: false, viewValue: 'No'},
  ];
  constructor(public studentService: StudentService) {
  }
  log(x) {console.log(x); }
  onSubmit() {
    console.log(this.studentModel);
    if (this.studentService.CreateStudent(this.studentModel).subscribe(res => {console.log('estudiante agregado'); })){
      alert(this.studentModel.nombre + ' ha sido agregado correctamente al sistema');
      window.location.reload();
    } else {
      alert('Ha ocurrido un error al intentar ingresar a ' + this.studentModel.nombre);
    }
  }
}


