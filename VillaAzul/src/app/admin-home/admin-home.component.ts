import { Component, OnInit } from '@angular/core';
import {UserService} from "../services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.css']
})
export class AdminHomeComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) {
    if (!this.userService.getUserLoggedIn() || this.userService.getUserLoggedIn().username !== 'admin') {
      this.router.navigateByUrl('/login');
    }
  }
  ngOnInit() {

  }

}
